let attackSound;
let hurtSound;
let jumpSound;
let walkSound;
let duckSound;
let deathSound;
let defendSound;
let backgroundMusic;
let muted = false;
let mutedMusic = true;

function setup() {
  attackSound = loadSound("sound/Attack2.mp3");
  attackSound.setVolume(0.05);
  hurtSound = loadSound("sound/painChirp.mp3");
  hurtSound.setVolume(0.5);
  jumpSound = loadSound("sound/Jump.mp3");
  jumpSound.setVolume(0.7);
  walkSound = loadSound("sound/walk2.mp3");
  walkSound.setVolume(0.1);
  duckSound = loadSound("sound/Cancel1.mp3");
  duckSound.setVolume(0.05);
  deathSound = loadSound("sound/Poison.mp3");
  deathSound.setVolume(0.5);
  defendSound = loadSound("sound/Stare.mp3");
  defendSound.setVolume(0.05);
  backgroundMusic = loadSound("sound/Battle8.mp3", loaded);
  backgroundMusic.setVolume(0);
}

function loaded() {
  backgroundMusic.play();
  backgroundMusic.loop();
}

const char1Div = document.createElement("div");
char1Div.id = "char1Div";
char1Div.className = "char";
const char1InternalDiv = document.createElement("div");
char1InternalDiv.id = "char1InternalDiv";
char1Div.appendChild(char1InternalDiv);

const char2Div = document.createElement("div");
char2Div.id = "char2Div";
char2Div.className = "char";
const char2InternalDiv = document.createElement("div");
char2InternalDiv.id = "char2InternalDiv";
char2Div.appendChild(char2InternalDiv);

const kid1Div = document.createElement("div");
kid1Div.id = "kid1Div";
const kid1InternalDiv = document.createElement("div");
kid1InternalDiv.id = "kid1InternalDiv";
kid1Div.appendChild(kid1InternalDiv);

const destinationForChar1 = document.getElementById("char1Storage");
destinationForChar1.appendChild(char1Div);
const destinationForChar2 = document.getElementById("char2Storage");
destinationForChar2.appendChild(char2Div);
const destinationForKid1 = document.getElementById("kid1Storage");
destinationForKid1.appendChild(kid1Div);

let positionLeftChar1 = 100;
let positionLeftChar2 = 275;

let gameOver = false;
let conditionsForSuccessfulAttack = false;

class Character {
  constructor(
    name,
    health,
    stamina,
    life,
    div,
    internalDiv,
    defending,
    jumping,
    ducking,
    positionLeft
  ) {
    this.name = name;
    this.health = health;
    this.stamina = stamina;
    this.life = life;
    this.div = div;
    this.internalDiv = internalDiv;
    this.defending = defending;
    this.jumping = jumping;
    this.ducking = ducking;
    this.positionLeft = positionLeft;
  }
  tired(x) {
    this.stamina = this.stamina - x;
    staminaBarLoseStamina(this.div);
    return this.stamina;
  }

  die() {
    if (this.health <= 0) {
      this.life = false;
      deathSound.play();
      return this.life;
    }
  }

  receiveDamage() {
    this.health = this.health - 10;
    healthBarLoseHealth();
    charHurtAnimation(this.div);
    this.die();
    return this.health;
  }

  jump() {
    this.jumping = true;
    setTimeout(() => {
      this.jumping = false;
    }, 500);
    this.tired(5);
  }

  defend() {
    this.defending = true;
    setTimeout(() => {
      this.defending = false;
    }, 500);
    this.tired(3);
  }

  duck() {
    this.ducking = true;
    setTimeout(() => {
      this.ducking = false;
    }, 500);
    this.tired(3);
  }

  rest() {
    if (this.stamina < 100) {
      this.stamina += 1;
    }
    staminaBarLoseStamina(this.div);
  }

  attackChar2(character2) {
    char1CanSuccessfullyAttackChar2();
    if (conditionsForSuccessfulAttack === true) {
      character2.health = character2.health - 10;
      healthBarLoseHealth();
      charHurtAnimation(character2.div);
      hurtSound.play();
    }
    char1AttackAnimation(character1.internalDiv);
    attackSound.play();
    this.tired(5);
    return character2.health;
  }

  attackChar1(character1) {
    char2CanSuccessfullyAttackChar1();
    if (conditionsForSuccessfulAttack === true) {
      character1.health = character1.health - 10;
      healthBarLoseHealth();
      charHurtAnimation(character1.div);
      hurtSound.play();
    }
    char2AttackAnimation(character2.internalDiv);
    attackSound.play();
    this.tired(5);
    return character1.health;
  }
}

const character1 = new Character(
  "Green",
  100,
  100,
  true,
  char1Div,
  char1InternalDiv,
  false,
  false,
  false,
  100
);

const character2 = new Character(
  "Yellow",
  100,
  100,
  true,
  char2Div,
  char2InternalDiv,
  false,
  false,
  false,
  275
);

setInterval(function() {
  character1.rest();
  character2.rest();
}, 1000);

class ChildOfCharacter extends Character {
  constructor(
    childname,
    name,
    health,
    stamina,
    life,
    div,
    internalDiv,
    defending,
    jumping,
    ducking,
    positionLeft
  ) {
    super(
      name,
      health,
      stamina,
      life,
      div,
      internalDiv,
      defending,
      jumping,
      ducking,
      positionLeft
    );
    this.childname = childname;
  }

  getChildname() {
    return this.childname;
  }
}

const kid1 = new ChildOfCharacter(
  "Junior",
  "Blue",
  30,
  30,
  true,
  kid1Div,
  kid1InternalDiv,
  false,
  false,
  false,
  0
);

window.addEventListener("keydown", keyStroke);

function keyStroke(e) {
  if (gameOver === false) {
    if (character2.stamina > 0) {
      if (e.code === "ArrowRight") {
        if (positionLeftChar2 < 375) {
          walkSound.play();
          animateMoveRight(char2Div);
          moveChar2(+1);
          character2.tired(1);
        }
      }
      if (e.code === "ArrowLeft") {
        if (positionLeftChar2 > 0) {
          walkSound.play();
          animateMoveLeft(char2Div);
          moveChar2(-1);
          character2.tired(1);
        }
      }
      if (e.code === "ArrowUp") {
        animateChar2Jump(char2Div);
        character2.jump();
      }
      if (e.code === "ArrowDown") {
        duckSound.play();
        animateDuck(char2Div);
        character2.duck();
      }
      if (e.code === "Slash") {
        if (positionLeftChar2 > 25) {
          animateJumpLeftChar2();
          jumpSound.play();
          moveChar2(-2);
          character2.jump();
        }
      }
      if (e.code === "ShiftRight") {
        if (positionLeftChar2 < 350) {
          animateJumpRightChar2();
          jumpSound.play();
          moveChar2(+2);
          character2.jump();
        }
      }
      if (e.code === "Backslash") {
        animateDefend(char2Div);
        defendSound.play();
        character2.defend();
      }
      if (e.code === "Enter") {
        if (
          character2.jumping === false &&
          character2.ducking === false &&
          character2.defending === false
        ) {
          character2.attackChar1(character1);
        }
      }
    }
    if (character1.stamina > 0) {
      if (e.code === "KeyD") {
        if (positionLeftChar1 < 375) {
          walkSound.play();
          animateMoveRight(char1Div);
          moveChar1(+1);
          character1.tired(1);
        }
      }
      if (e.code === "KeyA") {
        if (positionLeftChar1 > 0) {
          walkSound.play();
          animateMoveLeft(char1Div);
          moveChar1(-1);
          character1.tired(1);
        }
      }
      if (e.code === "KeyW") {
        animateChar1Jump(char1Div);
        character1.jump();
      }
      if (e.code === "KeyE") {
        if (positionLeftChar1 < 350) {
          animateJumpRightChar1();
          jumpSound.play();
          moveChar1(+2);
          character1.jump();
        }
      }
      if (e.code === "KeyQ") {
        if (positionLeftChar1 > 25) {
          animateJumpLeftChar1();
          jumpSound.play();
          moveChar1(-2);
          character1.jump();
        }
      }
      if (e.code === "KeyS") {
        duckSound.play();
        animateDuck(char1Div);
        character1.duck();
      }
      if (e.code === "KeyB") {
        animateDefend(char1Div);
        defendSound.play();
        character1.defend();
      }
      if (e.code === "Space") {
        if (
          character1.jumping === false &&
          character1.ducking === false &&
          character1.defending === false
        ) {
          character1.attackChar2(character2);
        }
      }
    }
    checkWin();
    checkTired();
  }
}

function checkWin() {
  character1.die();
  character2.die();
  if (character1.life === false || character2.life === false) {
    gameOver = true;
    if (character1.life === false) {
      dyingAnimation(char1Div);
      char1Div.removeChild(char1InternalDiv);
      victoryDance(char2Div);
      document.getElementById("winnerText").innerHTML =
        character2.name + " Wins!";
    } else {
      dyingAnimation(char2Div);
      char2Div.removeChild(char2InternalDiv);
      victoryDance(char1Div);
      document.getElementById("winnerText").classList.add("enlargeText");
      document.getElementById("winnerText").innerHTML =
        character1.name + " Wins!";
    }
  }
}

function checkTired() {
  if (character1.stamina <= 10) {
    animateTired(char1Div);
  }
  if (character2.stamina <= 10) {
    animateTired(char2Div);
  }
}

function healthBarLoseHealth() {
  document.getElementById("char1Health").style.width = character1.health + "px";
  document.getElementById("char2Health").style.width = character2.health + "px";
}

function staminaBarLoseStamina() {
  document.getElementById("char1Stamina").style.width =
    character1.stamina + "px";
  document.getElementById("char2Stamina").style.width =
    character2.stamina + "px";
}

function moveChar1(x) {
  positionLeftChar1 = positionLeftChar1 + 25 * x;
  document.getElementById("char1Div").style.left = positionLeftChar1 + "px";
  return positionLeftChar1;
}

function moveChar2(x) {
  positionLeftChar2 = positionLeftChar2 + 25 * x;
  document.getElementById("char2Div").style.left = positionLeftChar2 + "px";
  return positionLeftChar2;
}

function char1CanSuccessfullyAttackChar2() {
  if (
    character2.defending === false &&
    character2.jumping === false &&
    character2.ducking === false
  ) {
    if (positionLeftChar2 >= positionLeftChar1) {
      if (positionLeftChar2 - positionLeftChar1 <= 50) {
        return (conditionsForSuccessfulAttack = true);
      } else {
        return (conditionsForSuccessfulAttack = false);
      }
    }
    if (positionLeftChar1 > positionLeftChar2) {
      if (positionLeftChar1 - positionLeftChar2 <= 50) {
        return (conditionsForSuccessfulAttack = true);
      } else {
        return (conditionsForSuccessfulAttack = false);
      }
    }
  } else {
    return (conditionsForSuccessfulAttack = false);
  }
}

function char2CanSuccessfullyAttackChar1() {
  if (
    character1.defending === false &&
    character1.jumping === false &&
    character1.ducking === false
  ) {
    if (positionLeftChar2 >= positionLeftChar1) {
      if (positionLeftChar2 - positionLeftChar1 <= 50) {
        return (conditionsForSuccessfulAttack = true);
      } else {
        return (conditionsForSuccessfulAttack = false);
      }
    }
    if (positionLeftChar1 > positionLeftChar2) {
      if (positionLeftChar1 - positionLeftChar2 <= 50) {
        return (conditionsForSuccessfulAttack = true);
      } else {
        return (conditionsForSuccessfulAttack = false);
      }
    }
  } else {
    return (conditionsForSuccessfulAttack = false);
  }
}

function charHurtAnimation(charDiv) {
  charDiv.classList.add("hurt");
  setTimeout(function() {
    charDiv.classList.remove("hurt");
  }, 100);
}

function dyingAnimation(charDiv) {
  charDiv.classList.add("death");
  setTimeout(function() {
    charDiv.classList.remove("death");
  }, 100000);
}

function char1AttackAnimation() {
  char1InternalDiv.classList.add("attack");
  setTimeout(function() {
    char1InternalDiv.classList.remove("attack");
  }, 300);
}

function char2AttackAnimation() {
  char2InternalDiv.classList.add("attack");
  setTimeout(function() {
    char2InternalDiv.classList.remove("attack");
  }, 300);
}

function victoryDance(x) {
  x.classList.add("victoryDance");
}

function animateMoveRight(x) {
  x.classList.add("slideRight");
  setTimeout(function() {
    x.classList.remove("slideRight");
  }, 300);
}

function animateMoveLeft(x) {
  x.classList.add("slideLeft");
  setTimeout(function() {
    x.classList.remove("slideLeft");
  }, 300);
}

function animateChar1Jump(x) {
  jumpSound.play();
  x.classList.add("jumpChar1");
  setTimeout(function() {
    x.classList.remove("jumpChar1");
  }, 500);
}

function animateChar2Jump(x) {
  jumpSound.play();
  x.classList.add("jumpChar2");
  setTimeout(function() {
    x.classList.remove("jumpChar2");
  }, 500);
}

function animateJumpRightChar1() {
  char1Div.classList.add("jumpRightChar1");
  setTimeout(function() {
    char1Div.classList.remove("jumpRightChar1");
  }, 500);
}

function animateJumpLeftChar1() {
  char1Div.classList.add("jumpLeftChar1");
  setTimeout(function() {
    char1Div.classList.remove("jumpLeftChar1");
  }, 500);
}

function animateJumpRightChar2() {
  char2Div.classList.add("jumpRightChar2");
  setTimeout(function() {
    char2Div.classList.remove("jumpRightChar2");
  }, 500);
}

function animateJumpLeftChar2() {
  char2Div.classList.add("jumpLeftChar2");
  setTimeout(function() {
    char2Div.classList.remove("jumpLeftChar2");
  }, 500);
}

function animateDuck(x) {
  x.classList.add("duck");
  setTimeout(function() {
    x.classList.remove("duck");
  }, 500);
}

function animateDefend(charDiv) {
  charDiv.classList.add("block");
  setTimeout(function() {
    charDiv.classList.remove("block");
  }, 1000);
}

function animateTired(x) {
  x.classList.add("tired");
  setTimeout(function() {
    x.classList.remove("tired");
  }, 20000);
}

document.getElementById("char1Name").innerHTML = character1.name;
document.getElementById("char2Name").innerHTML = character2.name;

const resetButton = document.getElementById("resetButton");
resetButton.onclick = function() {
  location.reload();
};

const muteButton = document.getElementById("muteButton");
muteButton.onclick = function() {
  if (muted === false) {
    muteButton.innerHTML = "Unmute Sounds";
    attackSound.setVolume(0);
    hurtSound.setVolume(0);
    jumpSound.setVolume(0);
    walkSound.setVolume(0);
    muted = true;
  } else {
    muteButton.innerHTML = "Mute Sounds";
    attackSound.setVolume(0.05);
    hurtSound.setVolume(0.5);
    jumpSound.setVolume(0.7);
    walkSound.setVolume(0);
    muted = false;
  }
};

const muteMusicButton = document.getElementById("muteMusicButton");
muteMusicButton.onclick = function() {
  if (mutedMusic === false) {
    muteMusicButton.innerHTML = "Unmute Music";

    backgroundMusic.setVolume(0);
    mutedMusic = true;
  } else if (mutedMusic === true) {
    muteMusicButton.innerHTML = "Mute Music";
    backgroundMusic.setVolume(0.25);
    mutedMusic = false;
  }
};
